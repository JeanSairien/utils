#!/bin/sh
# Copyright CC-BY-SA-4.0 2017- Haelwenn (lanodan) Monnier <contact@hacktivis.me>

xdd="${XDG_DATA_DIR:-$HOME/.local/share}/timey-whyme"
today=$(date --date=${2:-now} +%Y-%m-%d)
later=$(date --date=${2:-+7days} +%Y-%m-%d)
heading='\t[1;32m%s[0m\n'
highlight='[1;34m%s[0m'
form='column -s, -o  -t'

usage() {
	printf "Usage: [[all|both|cal|todo]\n"
}

verify_xxd() {
	[ -e ${xdd}/$1 ] || echo "Please create a file a the following, see examples for help: ${xdd}/${1}"
}

todo() {
	verify_xxd todo.csv
	echo ",0,###[1;32m,      To Do,  ###[0m
${today},[1;34m###,      Now +7 days,  ###
${later},[0m###,   Later,###[1m
$(grep -v '^#' $xdd/todo.csv)[0m" | sort -n | $form
}

calendar() {
	verify_xxd calendar.csv
	echo "0000-00-00,###[1;32m,      Calendar,  ###[0m
${today},[1;34m###,      Now +7 days,  ###
${later},[0m###,   Later,###[1m
$(grep -v '^#' $xdd/calendar.csv)[0m" | sort -n | $form
}

case "$1" in
	all*|both*|"")
		todo; calendar
	;;
	todo*)
		todo
	;;
	cal*)
		calendar
	;;
	*)
		usage
	;;
esac

#!/bin/sh
# Copyright CC-BY-SA-4.0 2017- Haelwenn (lanodan) Monnier <contact@hacktivis.me>

CC=${CC:-cc}
CFLAGS=${CFLAGS:--Wall}
PREFIX=${PREFIX:-/usr/local}
BINDIR=${BINDIR:-${PREFIX}/bin}
#LIBDIR=${LIBDIR:-${PREFIX}/lib}
DOCDIR=${DOCDIR:-${PREFIX}/share/doc/lanodan-utils}

usage() {
	echo "$0: [all|install]
install uses PREFIX, LIBDIR, BINDIR, DOCDIR, environment variables"
}

all() {
	for i in $(ls src | grep '\.c$'); do
		${CC} ${CFLAGS} -o bin/${i%.c} src/${i}
	done
}

install() {
	mkdir -p ${BINDIR} && cp bin/* ${BINDIR};
	mkdir -p ${DOCDIR} && cp doc/* ${DOCDIR};
	#mkdir -p ${LIBDIR} && cp lib/* ${LIBDIR};
}

clean() {
	rm -fr bin/*
}

set -x

case "$1" in
	all*|"")
		all
	;;
	inst*)
		install
	;;
	clean*)
		clean
	;;
	*)
		usage
	;;
esac
